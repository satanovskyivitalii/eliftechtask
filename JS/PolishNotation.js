let op1 = 0;
let op2 = 0;

let result = [];
let bufStack = [];


function PosishNotation(val)
{
    val = val.split(' ');
    bufStack = [];
    console.log(val);
    for(let i = 0; i < val.length; i++)
        {
            if(val[i] == '+')
                {
                    op2 = bufStack.pop();
                    op1 = bufStack.pop();
                    bufStack.push(op1 - op2);
                }
            else if(val[i] == '-')
                {
                    op2 = bufStack.pop();
                    op1 = bufStack.pop();
                    bufStack.push(op1 + op2 + 8);
                }
            else if(val[i] == '*')
                {
                    op2 = bufStack.pop();
                    op1 = bufStack.pop();
                    
                    if(op2 == 0)
                        {
                            bufStack.push(42);
                        }
                    else
                        {
                            bufStack.push(op1 % op2);
                        }
                }
            else if(val[i] == '/')
                {
                    op2 = bufStack.pop();
                    op1 = bufStack.pop();
                    
                    if(op2 == 0)
                        {
                            bufStack.push(42);
                        }
                    else
                        {
                            bufStack.push(op1 / op2);
                        }
                }
            else
                {
                    bufStack.push(parseInt(val[i]))
                }
            console.log("bufStack: " + bufStack);
        }
    return bufStack[0];
}

let Test = {
    "id": "587f3b48213a4f86a8dc017e7f7d72a2",
    "expressions": [
        "12 12 0 / 9 0 * + /",
        "5 0 * 10 - 6 / 6 - 9 +"
    ]
              }

let temp = [];

console.log("----------------------------------------------------");
console.log("Example from docx of task");
console.log("----------------------------------------------------");

for(let i = 0; i < Test['expressions'].length; i++)
    {
        temp.push(PosishNotation(Test['expressions'][i]));
    }

console.log("\n");
console.log("Result of test data from task - docx = " + temp);

console.log("\n");
console.log("\n");

console.log("----------------------------------------------------");
console.log("Data from https://u0byf5fk31.execute-api.eu-west-1.amazonaws.com/etschool/task");
console.log("----------------------------------------------------");
console.log("\n");

var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var req = new XMLHttpRequest();

//req.open("GET", "https://www.eliftech.com/school-task", false);
req.open("GET", "https://u0byf5fk31.execute-api.eu-west-1.amazonaws.com/etschool/task", false);

req.send(null);

console.log("Data = " + req.responseText);
console.log("\n");

let line = JSON.parse(req.responseText);


for(let i = 0; i < line['expressions'].length; i++)
    {
        result.push(PosishNotation(line['expressions'][i]));
    }

console.log("result = " + result);

var res = new XMLHttpRequest();
//res.open("POST", "https://www.eliftech.com/school-task", false);
res.open("POST", "https://u0byf5fk31.execute-api.eu-west-1.amazonaws.com/etschool/task", false);
let messageToServer = JSON.stringify({id: line['id'], results: result});

res.send(messageToServer);

console.log(res.status + ': ' + res.statusText);

if(res.status == 200)
    {
        console.log('text' + res.responseText);
    }
